<?php

class FME_Copycategories_Block_Adminhtml_Copycategories_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'copycategories';
        $this->_controller = 'adminhtml_copycategories';
        
        $this->_updateButton('save', 'label', Mage::helper('copycategories')->__('Save Item'));
        $this->_updateButton('delete', 'label', Mage::helper('copycategories')->__('Delete Item'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('copycategories_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'copycategories_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'copycategories_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('copycategories_data') && Mage::registry('copycategories_data')->getId() ) {
            return Mage::helper('copycategories')->__("Edit Item '%s'", $this->htmlEscape(Mage::registry('copycategories_data')->getTitle()));
        } else {
            return Mage::helper('copycategories')->__('Add Item');
        }
    }
}