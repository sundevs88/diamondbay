<?php
class FME_Copycategories_Block_Adminhtml_Catalog_Category_Edit_Form extends Mage_Adminhtml_Block_Catalog_Category_Edit_Form
{
    
  protected function  _prepareLayout()
    {
           parent::_prepareLayout();
              $category = $this->getCategory();
        $categoryId = (int) $category->getId(); // 0 when we create category, otherwise some value for editing category

        $this->setChild('tabs',
            $this->getLayout()->createBlock('adminhtml/catalog_category_tabs', 'tabs')
        );
           if ($categoryId > 0 ) {
           $this->addAdditionalButton('update_button',  array('name' => 'update_button','title'=>'Copy Category','type'=>"button",'label'=> Mage::helper('catalog')->__('Copy Category'),
            'onclick'   => "location.href = '".$this->getUrl('copycategories/adminhtml_copycategories/copycategory/',array('id' => $categoryId))."'"));

        return parent::_prepareLayout();
    }
    }

    
 
}