<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PENDINGREGISTRATION
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

class Itoris_PendingRegistration_Block_Tab
	extends Mage_Adminhtml_Block_Widget_Form
	implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
	public function initForm() {
		$form = new Varien_Data_Form();
		$form->setHtmlIdPrefix('_account_admin');
		$form->setFieldNameSuffix('account_admin');

		$usersTableName = Mage::getSingleton( 'core/resource' )->getTableName( 'itoris_pendingregistration_users' );
		
		$customer = Mage::registry('current_customer');

		$db = Mage::getSingleton( 'core/resource' )->getConnection( 'core_write' );
		$email = $customer->getEmail();
		$result = $db->query( 'SELECT * FROM '.$usersTableName.' WHERE customer_id='.$db->quote( $customer->getId() ) );
		$data = $result->fetch();
		if (!$data) {
			/** @var $helper Itoris_PendingRegistration_Helper_Data */
			$helper = Mage::helper('itoris_pendingregistration/data');
			$scope = $helper->getCustomerScope($customer);
			$status = Itoris_PendingRegistration_Model_Settings::inst()->isEngineActive($scope) ? Itoris_PendingRegistration_Model_Users::STATUS_PENDING : Itoris_PendingRegistration_Model_Users::STATUS_APPROVED;
			$db->query( 'INSERT INTO `'.$usersTableName.'` SET customer_id='.$db->quote($customer->getId()).', status=' . $status );
			$result = $db->query( 'SELECT status FROM '.$usersTableName.' WHERE customer_id='.$db->quote( $customer->getId() ) );
			$data = $result->fetch();
		}
		
		$fieldset = $form->addFieldset('status_fieldset',
			array('legend'=>$this->__('Customer status'))
		);

		$isNotConfirmed = ($customer->getConfirmation() && $customer->isConfirmationRequired()) || (isset($data['status']) && $data['status'] == Itoris_PendingRegistration_Model_Users::STATUS_NOT_CONFIRMED_BY_EMAIL)
							? true
							: false;

		if ($customer->getConfirmation() && $customer->isConfirmationRequired()) {
			$values = array(
				Itoris_PendingRegistration_Model_Users::STATUS_NOT_CONFIRMED_BY_EMAIL => '',
			);
		} else {
			$values = array(
				$this->__('Pending'),
				$this->__('Approved'),
				$this->__('Declined'),
			);
		}
		$fieldset->addField( 'status', 'select', array(
			'name' 	   => 'status',
			'label'    => $this->__('Customer status'),
			'values'   => $values,
			'value'	   => 1,
			'disabled' => $isNotConfirmed,
			'note'     => $isNotConfirmed ? $this->__('The account is not yet confirmed by the customer. You can manually activate it on the Account Information tab') : '',
		));

		$form->setValues($data);
		$this->setForm($form);
		return $this;
	}

	protected function _toHtml() {
		$this->initForm();
		return parent::_toHtml();
	}

	public function getTabLabel() {
		return $this->__('Account activation');
	}

	public function getTabTitle() {
		return $this->getTabLabel();
	}

	public function canShowTab() {
		return (bool)Mage::registry('current_customer')->getId();
	}

	public function isHidden() {
		return false;
	}
}

?>