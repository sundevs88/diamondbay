<?php

class FME_Copycategories_Model_Mysql4_Copycategories_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('copycategories/copycategories');
    }
}