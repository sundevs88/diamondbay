<?php
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PRODUCTPRICEVISIBILITY
 * @copyright  Copyright (c) 2013 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */


class Itoris_ProductPriceVisibility_Helper_Product extends Itoris_ProductPriceVisibility_Helper_Data {

	const MODE_DEFAULT = 'default';
	const MODE_NO_PRICE = 'no_price';
	const MODE_OUT_OF_STOCK = 'out_of_stock';
	const MODE_CUSTOM_MESSAGE = 'custom_message';
	const MODE_SHOW_PRICE_DISALLOW_ADD_TO_CART = 'show_price_disallow_add_to_cart';

	protected $productSettings = array();
	protected $categorySettings = array();

	/**
	 * @param $productId
	 * @return Itoris_ProductPriceVisibility_Model_Settings
	 */
	protected function getSettingsForProduct($productId, $storeId) {
		if (!isset($this->productSettings[$productId])) {
			$this->productSettings[$productId] = Mage::getModel('itoris_productpricevisibility/settings')->load(0, $storeId, $productId);
		}
		return $this->productSettings[$productId];
	}
	protected function getSettingsForCategory($categoryId, $storeId) {
		if (!isset($this->productSettings[$categoryId])) {
			$this->productSettings[$categoryId] = Mage::getModel('itoris_productpricevisibility/settings')->load(0, $storeId, 0, $categoryId);
		}
		return $this->productSettings[$categoryId];
	}

	public function getPriceVisibilityConfig($product, $storeId = null) {
		if (is_null($storeId)) {
			$storeId = (int)Mage::app()->getStore()->getId();
		}
		$priceGroupId = $this->getGroupId('itoris_productpricevisibility_price_visibility_group', $product->getId(), $storeId);
		$productSettings = $this->getSettingsForProduct($product->getId(), $storeId);
		$config = array(
			'mode'    => self::MODE_DEFAULT,
			'message' => '',
		);
		if (Mage::registry('is_current_category')) {
			$categorySettings = $this->getSettingsForCategory($product->getCategoryId(), $storeId);
			$categoryGroupId = $this->getGroupIdForCategory('itoris_productpricevisibility_category_visibility_group', $product->getCategoryId(), $storeId);
			if ($this->isRegisteredFrontend() && $this->isRightConditions($categorySettings, $categoryGroupId, 'category')) {
				if ($categorySettings->getCategoryHidingMode() == Itoris_ProductPriceVisibility_Block_Admin_Category_Edit_Tab_VisibilityRules::NO_PRICE) {
					$config['mode'] = self::MODE_NO_PRICE;
				} elseif ($categorySettings->getCategoryHidingMode() == Itoris_ProductPriceVisibility_Block_Admin_Category_Edit_Tab_VisibilityRules::CUSTOM_MESSAGE) {
					$config['mode'] = self::MODE_CUSTOM_MESSAGE;
					$config['message'] = $categorySettings->getCategoryHidingMessage();
				} elseif ($categorySettings->getCategoryHidingMode() == Itoris_ProductPriceVisibility_Block_Admin_Category_Edit_Tab_VisibilityRules::SHOW_OUT_OF_STOCK) {
					$config['mode'] = self::MODE_OUT_OF_STOCK;
				} elseif ($categorySettings->getCategoryHidingMode() == Itoris_ProductPriceVisibility_Block_Admin_Category_Edit_Tab_VisibilityRules::SHOW_PRICE_DISALLOW_ADD_TO_CART) {
					$config['mode'] = self::MODE_SHOW_PRICE_DISALLOW_ADD_TO_CART;
					$config['message'] = $categorySettings->getCategoryRestrictionMessage();
				}
			}
		}
		if ($this->isRegisteredFrontend()) {
			if ($this->isRightConditions($productSettings, $priceGroupId, 'price')) {
				if ($productSettings->getPriceHidingMode() == Itoris_ProductPriceVisibility_Block_Admin_Product_Edit_Tab_PriceVisibility::PRICE_NO_PRICE) {
					$config['mode'] = self::MODE_NO_PRICE;
				} elseif ($productSettings->getPriceHidingMode() == Itoris_ProductPriceVisibility_Block_Admin_Product_Edit_Tab_PriceVisibility::SHOW_CUSTOM_MESSAGE) {
					$config['mode'] = self::MODE_CUSTOM_MESSAGE;
					$config['message'] = $productSettings->getPriceHidingMessage();
				} elseif ($productSettings->getPriceHidingMode() == Itoris_ProductPriceVisibility_Block_Admin_Product_Edit_Tab_PriceVisibility::PRICE_SHOW_OUT_OF_STOCK) {
					$config['mode'] = self::MODE_OUT_OF_STOCK;
				} elseif ($productSettings->getPriceHidingMode() == Itoris_ProductPriceVisibility_Block_Admin_Product_Edit_Tab_PriceVisibility::SHOW_PRICE_DISALLOW_ADD_TO_CART) {
					$config['mode'] = self::MODE_SHOW_PRICE_DISALLOW_ADD_TO_CART;
					$config['message'] = $productSettings->getPriceRestrictionMessage();
				}
			} else {
				$categoryIds = $product->getCategoryIds();
				$modePriceCategory = $this->getDataHelper()->rulesForCategory($categoryIds, $storeId, 'price');
				$modeOutOfStockCategory = $this->getDataHelper()->rulesForCategory($categoryIds, $storeId, 'out_of_stock');
				if (is_array($modePriceCategory) && !empty($modePriceCategory)) {
					if ($modePriceCategory[0] == 'itoris_price') {
						$config['mode'] = self::MODE_NO_PRICE;
					} elseif ($modePriceCategory[0] == 'custom_message') {
						$config['mode'] = self::MODE_CUSTOM_MESSAGE;
						$config['message'] = $modePriceCategory[1];
					} elseif ($modePriceCategory[0] == 'restriction_message') {
						$config['mode'] = self::MODE_SHOW_PRICE_DISALLOW_ADD_TO_CART;
						$config['message'] = $modePriceCategory[1];
					}
				}
				if ($modeOutOfStockCategory){
					$config['mode'] = self::MODE_OUT_OF_STOCK;
				}
			}
		}

		return $config;
	}

	public function getHideVisibilityConfig($ids) {
		if ($this->getDataHelper()->isRegisteredFrontend()) {
			$checkStore = (int)Mage::app()->getStore()->getId();
			$productVisibilityModel = Mage::getModel('itoris_productpricevisibility/settings');
			$settingsByProductId = $productVisibilityModel->loadSettingsForProduct(0, $checkStore);
			$productIdHide = array();
			$resource = Mage::getSingleton('core/resource');
			$connection = $resource->getConnection('read');
			$categoryTable = $resource->getTableName('catalog_category_product_index');
			$productIds = $ids;
			foreach ($productIds as $idForLoad) {
				$categoryIds = array();
				$productId = (int)$idForLoad['entity_id'];
				$settings = $productVisibilityModel->load(0, $checkStore, $productId);
				$categoryIdsFromTable = $connection->fetchAll("select category_id from {$categoryTable} where product_id = {$productId}");
				foreach ($categoryIdsFromTable as $ids) {
					$categoryIds[] = $ids['category_id'];
				}
				if ($this->getDataHelper()->rulesForCategory($categoryIds, $checkStore, 'hide')) {
					$priceGroupId = $this->getDataHelper()->getGroupId('itoris_productpricevisibility_price_visibility_group', $productId, $checkStore);
					if (!$this->getDataHelper()->customerGroup($priceGroupId)
						&& $this->getDataHelper()->isVisibleByRestrictionDate($settings->getPriceRestrictionBegin(), $settings->getPriceRestrictionEnd())
					) {
						$productIdHide[] = $productId;
					}
				}
			}
			foreach ($settingsByProductId as $id => $product) {
				$productId = (int)$id;
				$product = new Varien_Object($product);
				$productGroupId = $this->getDataHelper()->getGroupId('itoris_productpricevisibility_product_visibility_group', $productId, $checkStore);
				if ($product->getData('product_hiding_mode') == Itoris_ProductPriceVisibility_Block_Admin_Product_Edit_Tab_PriceVisibility::PRODUCT_HIDE_COMPLETELY
					&& $this->getDataHelper()->customerGroup($productGroupId)
					&& $this->getDataHelper()->isVisibleByRestrictionDate($product->getData('product_restriction_begin'), $product->getData('product_restriction_end'))
				) {
					$productIdHide[] = $productId;
				}
			}
			return $productIdHide;
		}
	}

	/**
	 * @return Itoris_ProductPriceVisibility_Helper_Data
	 */

	public function getDataHelper() {
		return Mage::helper('itoris_productpricevisibility');
	}
}
?>