<?php

class FME_Copycategories_Model_Mysql4_Copycategories extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the copycategories_id refers to the key field in your database table.
        $this->_init('copycategories/copycategories', 'copycategories_id');
    }
}