<?php

class QuyenDam_ComingSoon_Model_Observer extends Varien_Object {
    public function salesQuoteItemSetComingSoon($observer)
    {
        $quoteItem = $observer->getQuoteItem();
        $product = $observer->getProduct();
        $quoteItem->setComingSoon($product->getComingSoon());
    }
}