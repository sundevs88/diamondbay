<?php
/**
 * Created by PhpStorm.
 * User: quyendam
 * Date: 1/9/15
 * Time: 5:45 PM
 */ 
/* @var $installer Mage_Sales_Model_Resource_Setup */
$installer = new Mage_Sales_Model_Resource_Setup('core_setup');

$installer->startSetup();

$entities = array(
    'quote',
    'quote_item',
    'order',
    'order_item'
);
$options = array(
    'type'     => Varien_Db_Ddl_Table::TYPE_VARCHAR,
    'visible'  => true,
    'required' => false
);
foreach ($entities as $entity) {
    $installer->addAttribute($entity, 'coming_soon', $options);
}

$installer->endSetup();