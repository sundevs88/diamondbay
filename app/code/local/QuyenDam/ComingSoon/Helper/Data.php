<?php
/**
 * Created by PhpStorm.
 * User: quyendam
 * Date: 1/9/15
 * Time: 5:45 PM
 */ 
class QuyenDam_ComingSoon_Helper_Data extends Mage_Core_Helper_Abstract {
    public function getQtyIncrements( $item ) {
        $productId = $item->getProductId();
        if( $productId ) {
            $productData = Mage::getModel('cataloginventory/stock_item')->load($productId);
            $qtyIncrements = $productData->getQtyIncrements();
            if( $qtyIncrements ) {
                return $qtyIncrements;
            }
        }
        return false;
    }
}