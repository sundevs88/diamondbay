<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PRODUCTPRICEVISIBILITY
 * @copyright  Copyright (c) 2013 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

  

$this->startSetup();

$this->run("

create table if not exists {$this->getTable('itoris_productpricevisibility_view')} (
	`view_id` int unsigned not null auto_increment primary key,
	`scope` enum('default', 'website', 'store') not null,
	`scope_id` int unsigned not null,
	unique(`scope`, `scope_id`)
) engine = InnoDB default charset = utf8;

create table if not exists {$this->getTable('itoris_productpricevisibility_settings')} (
	`setting_id` int unsigned not null auto_increment primary key,
	`view_id` int unsigned not null,
	`product_id` int(10) unsigned null,
	`category_id` int(10) unsigned null,
	`key` varchar(255) not null,
	`value` int unsigned not null,
	`type` enum('text', 'default') null,
	unique(`view_id`, `key`, `product_id`),
	CONSTRAINT `FK_ITORIS_PRODUCTPRICEVISIBILITY_SETTING_CATEGORY_ID` foreign key (`category_id`) references {$this->getTable('catalog_category_entity')} (`entity_id`) on delete cascade on update cascade,
	CONSTRAINT `FK_ITORIS_PRODUCTPRICEVISIBILITY_SETTING_VIEW_ID` foreign key (`view_id`) references {$this->getTable('itoris_productpricevisibility_view')} (`view_id`) on delete cascade on update cascade,
	CONSTRAINT `FK_ITORIS_PRODUCTPRICEVISIBILITY_SETTING_PRODUCT_ID` foreign key (`product_id`) references {$this->getTable('catalog_product_entity')} (`entity_id`) on delete cascade on update cascade
) engine = InnoDB default charset = utf8;

create table if not exists {$this->getTable('itoris_productpricevisibility_settings_text')} (
	`setting_id` int unsigned not null,
	`value` text not null,
	index(`setting_id`),
	CONSTRAINT `FK_ITORIS_PRODUCTPRICEVISIBILITY_SETTING_TEXT_SETTING_ID` foreign key (`setting_id`) references {$this->getTable('itoris_productpricevisibility_settings')} (`setting_id`) on delete cascade on update cascade
) engine = InnoDB default charset = utf8;

create table if not exists {$this->getTable('itoris_productpricevisibility_product_visibility_group')} (
	`product_id` int(10) unsigned not null,
	`group_id` smallint(5) unsigned,
	`store_id` int unsigned not null,
	CONSTRAINT `FK_ITORIS_PRODUCTPRICEVISIBILITY_PRODUCT_GROUP_PRODUCT_ID` foreign key (`product_id`) references {$this->getTable('catalog_product_entity')} (`entity_id`) on delete cascade on update cascade,
    CONSTRAINT `FK_ITORIS_PRODUCTPRICEVISIBILITY_PRODUCT_GROUP_GROUP_ID` foreign key (`group_id`) references {$this->getTable('customer_group')} (`customer_group_id`) on delete cascade on update cascade
) engine = InnoDB default charset = utf8;

create table if not exists {$this->getTable('itoris_productpricevisibility_price_visibility_group')} (
	`product_id` int(10) unsigned not null,
	`group_id` smallint(5) unsigned,
	`store_id` int unsigned not null,
	CONSTRAINT `FK_ITORIS_PRODUCTPRICEVISIBILITY_PRICE_GROUP_PRODUCT_ID` foreign key (`product_id`) references {$this->getTable('catalog_product_entity')} (`entity_id`) on delete cascade on update cascade,
    CONSTRAINT `FK_ITORIS_PRODUCTPRICEVISIBILITY_PRICE_GROUP_GROUP_ID` foreign key (`group_id`) references {$this->getTable('customer_group')} (`customer_group_id`) on delete cascade on update cascade
) engine = InnoDB default charset = utf8;

create table if not exists {$this->getTable('itoris_productpricevisibility_category_visibility_group')} (
	`category_id` int(10) unsigned not null,
	`group_id` smallint(5) unsigned null,
	`store_id` int unsigned not null,
	CONSTRAINT `FK_ITORIS_PRODUCTPRICEVISIBILITY_CATEGORY_GROUP_CATEGORY_ID` foreign key (`category_id`) references {$this->getTable('catalog_category_entity')} (`entity_id`) on delete cascade on update cascade,
    CONSTRAINT `FK_ITORIS_PRODUCTPRICEVISIBILITY_CATEGORY_GROUP_GROUP_ID` foreign key (`group_id`) references {$this->getTable('customer_group')} (`customer_group_id`) on delete cascade on update cascade
) engine = InnoDB default charset = utf8;

");

$this->endSetup();
?>