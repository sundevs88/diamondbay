<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PRODUCTPRICEVISIBILITY
 * @copyright  Copyright (c) 2013 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

  

class Itoris_ProductPriceVisibility_Block_Admin_Settings_Edit_Form extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareForm() {
		$form = new Varien_Data_Form();

		$fieldset = $form->addFieldset('general_fields', array(
			'legend' => $this->__('Product and Price Visibility'),
		));

		$fieldset->addField('enabled', 'select', array(
			'name'   => 'settings[enabled][value]',
			'label'  => $this->__('Extension Enabled'),
			'title'  => $this->__('Extension Enabled'),
			'values' => array(
				array('label' => $this->__('Yes'),
					'value' => 1),
				array('label' => $this->__('No'),
					'value' => 0),
			),
		))->getRenderer()->setTemplate('itoris/productpricevisibility/configuration/form/element.phtml');

        $userGroups = Mage::getResourceModel('customer/group_collection')->toOptionArray();
        $fieldset->addField('global_user_groups', 'multiselect', array(
            'name'   => 'settings[global_user_groups][value]',
            'label'  => $this->__('All product are hidden to the following user groups (multi-select):'),
            'title'  => $this->__('All product are hidden to the following user groups (multi-select):'),
            'values' => $userGroups,
            'note'  => $this->__('CTRL + click to select/deselect group'),
        ));
        $fieldset->addField('global_restriction_begin', 'date', array(
            'name'		   => 'settings[global_restriction_begin][value]',
            'label'        => $this->__('Restriction by group begins on'),
            'title'        => $this->__('Restriction by group begins on'),
            'image'        => $this->getSkinUrl('images/grid-cal.gif'),
            'input_format' => Varien_Date::DATE_INTERNAL_FORMAT,
            'format'       => Varien_Date::DATE_INTERNAL_FORMAT,
            'class'        => 'validate-date-one-type'
        ));

        $fieldset->addField('global_restriction_end', 'date', array(
            'name'		   => 'settings[global_restriction_end][value]',
            'label'        => $this->__('Restriction by group ends on'),
            'title'        => $this->__('Restriction by group ends on'),
            'image'        => $this->getSkinUrl('images/grid-cal.gif'),
            'input_format' => Varien_Date::DATE_INTERNAL_FORMAT,
            'format'       => Varien_Date::DATE_INTERNAL_FORMAT,
            'class'        => 'validate-date-one-type'
        ));
        $fieldset->addField('global_hiding_mode', 'select', array(
            'name'		  => 'settings[global_hiding_mode][value]',
            'label'       => $this->__('Global hiding mode:'),
            'title'       => $this->__('Global hiding mode:'),
            'values' => array(
                array(
                    'label' => $this->__('Hide All Category and Products'),
                    'value' => Itoris_ProductPriceVisibility_Model_Settings::HIDE_ALL_GLOBAL,
                ),
                array(
                    'label' => $this->__('Hide all Products, leave Category visible'),
                    'value' => Itoris_ProductPriceVisibility_Model_Settings::HIDE_PRODUCT_GLOBAL,
                ),
                array(
                    'label' => $this->__('Hide All Prices'),
                    'value' => Itoris_ProductPriceVisibility_Model_Settings::HIDE_PRICES_GLOBAL,
                ),
                array(
                    'label' => $this->__('Show "Out of Stock" status'),
                    'value' => Itoris_ProductPriceVisibility_Model_Settings::SHOW_OUT_OF_STOCK_GLOBAL,
                ),
                array(
                    'label' => $this->__('Show Custom Stock status'),
                    'value' => Itoris_ProductPriceVisibility_Model_Settings::SHOW_CUSTOM_STOCK_GLOBAL,
                ),
                array(
                    'label' => $this->__('Show Prices, disallow adding to Cart'),
                    'value' => Itoris_ProductPriceVisibility_Model_Settings::SHOW_PRICE_DISALLOW_ADD_TO_CART_GLOBAL,
                ),
            ),
            'onchange'    => "
				if (this.value == 5) {
					$('global_custom_stock_status_box').show();
					$('global_restriction_message_box').hide();
				} else if (this.value == 6) {
				    $('global_restriction_message_box').show();
					$('global_custom_stock_status_box').hide();
				} else {
				    $('global_restriction_message_box').hide();
					$('global_custom_stock_status_box').hide();
				}

			"
        ));
        $elementRenderer = new Itoris_ProductPriceVisibility_Block_Admin_Form_Renderer_Element();
        $fieldset->addField('global_custom_stock_status', 'text', array(
            'name'	       => 'settings[global_custom_stock_status][value]',
            'label'        => $this->__('Custom Message:'),
            'title'        => $this->__('Custom Message:'),
            'container_id' => 'global_custom_stock_status_box',
        ));

        $fieldset->addField('global_restriction_message', 'text', array(
            'name'	       => 'settings[global_restriction_message][value]',
            'label'        => $this->__('Restriction Message:'),
            'title'        => $this->__('Restriction Message:'),
            'container_id' => 'global_restriction_message_box',
        ))->setRenderer($elementRenderer);

		$form->addValues($this->getFormHelper()->prepareElementsValues($form));
		$form->setUseContainer(true);
		$form->setId('edit_form');
		$form->setAction($this->getUrl('itoris_productpricevisibility/admin_configuration/save', array('_current' => true)));
		$form->setMethod('post');
		$this->setForm($form);
	}

	/**
	 * @return Itoris_ProductPriceVisibility_Helper_Form
	 */
	public function getFormHelper() {
		return Mage::helper('itoris_productpricevisibility/form');
	}

}
?>