<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PRODUCTPRICEVISIBILITY
 * @copyright  Copyright (c) 2013 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

 

class Itoris_ProductPriceVisibility_Block_Admin_Product_Edit_Tab_PriceVisibility
	extends Mage_Adminhtml_Block_Catalog_Form
	implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
	const PRODUCT_HIDE_COMPLETELY = 1;
	const PRODUCT_SHOW_OUT_OF_STOCK = 2;
	const PRODUCT_NO_PRICE = 3;
	const PRICE_SHOW_OUT_OF_STOCK = 1;
	const PRICE_NO_PRICE = 2;
	const SHOW_CUSTOM_MESSAGE = 3;
	const SHOW_PRICE_DISALLOW_ADD_TO_CART = 4;

	protected function _construct() {
		parent::_construct();
		$this->_setAfter();
	}

	/**
	 * Set after price tab
	 *
	 * @return Itoris_ProductPriceVisibility_Block_Admin_Product_Edit_Tab_PriceVisibility
	 */
	protected function _setAfter() {
		/** @var $product Mage_Catalog_Model_Product */
		$product = Mage::registry('current_product');
		$attributes = $product->getAttributes();
		$altAttribute = null;
		/** @var $attribute Mage_Catalog_Model_Resource_Eav_Attribute */
		foreach ($attributes as $attribute) {
			if ($attribute->getAttributeCode() == 'price') {
				return $this->_setAfterAttribute($attribute);
			} else if ($attribute->getAttributeCode() == 'enable_googlecheckout') {
				$altAttribute = $attribute;
			}
		}
		if ($altAttribute) {
			return $this->_setAfterAttribute($altAttribute);
		}
		return $this;
	}

	public function _setAfterAttribute($attribute) {
		$info = $attribute->getAttributeSetInfo();
		if (is_array($info)) {
			foreach ($info as $set) {
				if (isset($set['group_id'])) {
					$this->setAfter('group_' . $set['group_id']);
					break;
				}
			}
		}
		return $this;
	}

	protected function _prepareLayout() {
		parent::_prepareLayout();
		Varien_Data_Form::setFieldsetElementRenderer(
			$this->getLayout()->createBlock('itoris_productpricevisibility/admin_form_renderer_product_element')
		);

		return $this;
	}

	public function getTabLabel() {
		return $this ->__('Product and Price Visibility Rules');
	}

	public function getTabTitle() {
		return $this ->__('Product and Price Visibility Rules');
	}

	public function canShowTab() {
		if($this->getDataHelper()->getSettings()->getEnabled() && Mage::helper('itoris_productpricevisibility')->isAdminRegistered()) {
			return true;
		} else {
			return false;
		}
	}

	public function isHidden() {
		return false;
	}

	protected function _prepareForm() {
		$form = new Varien_Data_Form();
		$productId = Mage::registry('current_product')->getId();
		$checkStore = Mage::app()->getRequest()->getParam('store');
		$productPriceSetting = Mage::getModel('itoris_productpricevisibility/settings')->load(0, $checkStore, $productId);
		$groupIdValue = $this->getGroupId('itoris_productpricevisibility_product_visibility_group', $productId, (int)$checkStore);

		$productVisibility = $form->addFieldset('product_visibility', array(
			'legend' => $this->__('Product Visibility Rules'),
		));
		$userGroups = Mage::getResourceModel('customer/group_collection')->toOptionArray();
		$productVisibility->addField('product_user_groups', 'multiselect', array(
			'name'   => 'itoris_product_rules[groups]',
			'label'  => $this->__('Product is hidden to the following user groups (multi-select):'),
			'title'  => $this->__('Product is hidden to the following user groups (multi-select):'),
			'values' => $userGroups,
			'note'  => $this->__('CTRL + click to select/deselect group'),
			'value'  => $groupIdValue,
			'show_checkbox' => $this->getDataHelper()->useDefault('itoris_productpricevisibility_product_visibility_group', $productId, $checkStore)
		));

		$productVisibility->addField('product_restriction_begin', 'date', array(
			'name'		   => 'itoris_product_rules[restriction_begin]',
			'label'        => $this->__('Restriction by group begins on'),
			'title'        => $this->__('Restriction by group begins on'),
			'image'        => $this->getSkinUrl('images/grid-cal.gif'),
			'input_format' => Varien_Date::DATE_INTERNAL_FORMAT,
			'format'       => Varien_Date::DATE_INTERNAL_FORMAT,
			'value'        => $productPriceSetting->getProductRestrictionBegin(),
			'show_checkbox' => $productPriceSetting->isParentValue('product_restriction_begin'),
            'class'        => 'validate-date-one-type'
		));

		$productVisibility->addField('product_restriction_end', 'date', array(
			'name'		   => 'itoris_product_rules[restriction_end]',
			'label'        => $this->__('Restriction by group ends on'),
			'title'        => $this->__('Restriction by group ends on'),
			'image'        => $this->getSkinUrl('images/grid-cal.gif'),
			'input_format' => Varien_Date::DATE_INTERNAL_FORMAT,
			'format'       => Varien_Date::DATE_INTERNAL_FORMAT,
			'value'        => $productPriceSetting->getProductRestrictionEnd(),
			'show_checkbox' => $productPriceSetting->isParentValue('product_restriction_end'),
            'class'        => 'validate-date-one-type'
		));
		$productVisibility->addField('product_hiding_mode', 'select', array(
			 'name'		   => 'itoris_product_rules[hiding_mode]',
			 'label'       => $this->__('Product hiding mode:'),
			 'title'       => $this->__('Product hiding mode:'),
			 'values' => array(
				 array(
					 'label' => $this->__('Hide completely'),
					 'value' => self::PRODUCT_HIDE_COMPLETELY,
				 ),
				/* array(
					 'label' => $this->__('Show "Out of Stock"'),
					 'value' => self::PRODUCT_SHOW_OUT_OF_STOCK,
				 ),
				 array(
					 'label' => $this->__('No Price'),
					 'value' => self::PRODUCT_NO_PRICE,
				 ),*/
			 ),
			'value'        => $productPriceSetting->getProductHidingMode(),
			'show_checkbox' => $productPriceSetting->isParentValue('product_hiding_mode')
		 ));

		$priceGroupIdValue = $this->getGroupId('itoris_productpricevisibility_price_visibility_group', $productId, (int)$checkStore);

		$priceVisibility = $form->addFieldset('price_visibility', array(
			'legend' => $this->__('Price Visibility Rules'),
		));

		$userGroups = Mage::getResourceModel('customer/group_collection')->toOptionArray();

		$priceVisibility->addField('price_user_groups', 'multiselect', array(
			'name'	 => 'itoris_price_rules[groups]',
			'label'  => $this->__('Price is hidden to the following user groups (multi-select):'),
			'title'  => $this->__('Price is hidden to the following user groups (multi-select):'),
			'values' => $userGroups,
			'note'  => $this->__('CTRL + click to select/deselect group'),
			'value'  => $priceGroupIdValue,
			'show_checkbox' => $this->getDataHelper()->useDefault('itoris_productpricevisibility_price_visibility_group', $productId, $checkStore)
		));

		$priceVisibility->addField('price_restriction_begin', 'date', array(
			'name'		   => 'itoris_price_rules[restriction_begin]',
			'label'        => $this->__('Restriction by group begins on'),
			'title'        => $this->__('Restriction by group begins on'),
			'image'        => $this->getSkinUrl('images/grid-cal.gif'),
			'input_format' => Varien_Date::DATE_INTERNAL_FORMAT,
			'format'       => Varien_Date::DATE_INTERNAL_FORMAT,
			'value'        => $productPriceSetting->getPriceRestrictionBegin(),
			'show_checkbox' => $productPriceSetting->isParentValue('price_restriction_begin'),
            'class'        => 'validate-date-one-type'
		));

		$priceVisibility->addField('price_restriction_end', 'date', array(
			'name'		   => 'itoris_price_rules[restriction_end]',
			'label'        => $this->__('Restriction by group ends on'),
			'title'        => $this->__('Restriction by group ends on'),
			'image'        => $this->getSkinUrl('images/grid-cal.gif'),
			'input_format' => Varien_Date::DATE_INTERNAL_FORMAT,
			'format'       => Varien_Date::DATE_INTERNAL_FORMAT,
			'value'        => $productPriceSetting->getPriceRestrictionEnd(),
			'show_checkbox' => $productPriceSetting->isParentValue('price_restriction_end'),
            'class'        => 'validate-date-one-type'
		));
		$priceVisibility->addField('price_hiding_mode', 'select', array(
			'name'		  => 'itoris_price_rules[hiding_mode]',
			'label'       => $this->__('Price hiding mode:'),
			'title'       => $this->__('Price hiding mode:'),
			'values' => array(
				array(
					'label' => $this->__('Show "Out of Stock"'),
					'value' => self::PRICE_SHOW_OUT_OF_STOCK,
				),
				array(
					'label' => $this->__('No Price'),
					'value' => self::PRICE_NO_PRICE,
				),
				array(
					'label' => $this->__('Show Custom Stock Status'),
					'value' => self::SHOW_CUSTOM_MESSAGE,
				),
				array(
					'label' => $this->__('Show Price, disallow adding to Cart'),
					'value' => self::SHOW_PRICE_DISALLOW_ADD_TO_CART,
				),
			),
			'value'        => $productPriceSetting->getPriceHidingMode(),
			'show_checkbox' => $productPriceSetting->isParentValue('price_hiding_mode'),
			'onchange'    => "
				if (this.value == 3) {
					$('itoris_hiding_message_box').show();
					$('itoris_restriction_message_box').hide();
					$('hiding_message').disabled = false;
				} else if (this.value == 4) {
					$('itoris_restriction_message_box').show();
					$('itoris_hiding_message_box').hide();
				} else {
					$('itoris_hiding_message_box').hide();
					$('itoris_restriction_message_box').hide();
					$('hiding_message').disabled = true;
				}
			"
		));
		$hidingMode = $productPriceSetting->getPriceHidingMode();
		$elementRenderer = new Itoris_ProductPriceVisibility_Block_Admin_Form_Renderer_Element();
		$isVisibleCustomMessage = $hidingMode == self::SHOW_CUSTOM_MESSAGE ? true : false;
		$disabled = $isVisibleCustomMessage ? false : true;
		$priceVisibility->addField('hiding_message', 'text', array(
			'name'	       => 'itoris_price_rules[hiding_message]',
			'label'		   => $this->__('Custom Stock Status:'),
			'title'		   => $this->__('Custom Stock Status:'),
			'value'		   => $productPriceSetting->getPriceHidingMessage(),
			'container_id' => 'itoris_hiding_message_box',
			'required'	   => true,
			'disabled'	   => $disabled,
			'is_visible'   => $isVisibleCustomMessage,
			'show_checkbox' => $productPriceSetting->isParentValue('price_hiding_message')
		))->setRenderer($elementRenderer);

		$isVisibleRestrictionMessage = $hidingMode == self::SHOW_PRICE_DISALLOW_ADD_TO_CART ? true : false;
		$priceVisibility->addField('itoris_restriction_message', 'text', array(
			'name'	       => 'itoris_price_rules[restriction_message]',
			'label'        => $this->__('Restriction Message:'),
			'title'        => $this->__('Restriction Message:'),
			'value'        => $productPriceSetting->getPriceRestrictionMessage(),
			'show_checkbox' => $productPriceSetting->isParentValue('price_restriction_message'),
			'container_id' => 'itoris_restriction_message_box',
			'is_visible'   => $isVisibleRestrictionMessage
		))->setRenderer($elementRenderer);

		$this->setForm($form);

	}

	protected function getGroupId($tableName, $productId, $checkStore) {
		$resource = Mage::getSingleton('core/resource');
		$connection = $resource->getConnection('read');
		$groupTable = $resource->getTableName($tableName);
		if ($this->getDataHelper()->useDefault($tableName, $productId, $checkStore)) {
			$groupIdByArray = $connection->fetchAll("select group_id from {$groupTable} where product_id={$productId} and store_id=0");
		} else {
			$groupIdByArray = $connection->fetchAll("select group_id from {$groupTable} where product_id={$productId} and store_id={$checkStore}");
		}
		$groupId = '';
		foreach ($groupIdByArray as $key => $value) {
			if ($key == count($groupIdByArray) - 1) {
				$groupId .= $value['group_id'];
			} else {
				$groupId .= $value['group_id'] . ', ';
			}
		}
		return $groupId;
	}

	/**
	 * @return Itoris_ProductPriceVisibility_Helper_Data
	 */

	public function getDataHelper() {
		return Mage::helper('itoris_productpricevisibility/data');
	}
}
?>