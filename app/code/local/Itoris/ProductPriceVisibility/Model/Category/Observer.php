<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PRODUCTPRICEVISIBILITY
 * @copyright  Copyright (c) 2013 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

 

class Itoris_ProductPriceVisibility_Model_Category_Observer {

	public function addTabCategory($obj) {
		$tab = $obj->getTabs();
		if($this->getDataHelper()->getSettings()->getEnabled() && Mage::helper('itoris_productpricevisibility')->isAdminRegistered()) {
			$tab->addTab('visibility_rules', array(
				'label'     => Mage::helper('catalog')->__('Visibility Rules'),
				'content'   => $tab->getLayout()->createBlock(
					'itoris_productpricevisibility/admin_category_edit_tab_visibilityRules'
				)->toHtml(),
			));
		}
	}

	public function saveCategory($object) {
		$category = $object->getCategory();
		if($this->getDataHelper()->getSettings()->getEnabled() && Mage::helper('itoris_productpricevisibility')->isAdminRegistered()) {
			$categoryId = $category->getId();
			if ($categoryId) {
				$checkStore = (int)Mage::app()->getRequest()->getParam('store');
				$categoryVisibility =  Mage::app()->getRequest()->getParam('itoris_category_visibility');
				$settings = array();
				if (isset($categoryVisibility['restriction_begin'])) {
					$settings['category_restriction_begin'] =  array('value' => $categoryVisibility['restriction_begin']);
				}
				if (isset($categoryVisibility['restriction_end'])) {
					$settings['category_restriction_end'] =  array('value' => $categoryVisibility['restriction_end']);
				}
				if (isset($categoryVisibility['hiding_mode'])) {
					$settings['category_hiding_mode'] =  array('value' => $categoryVisibility['hiding_mode']);
				}
				if (isset($categoryVisibility['hiding_message'])) {
					$settings['category_hiding_message'] =  array('value' => $categoryVisibility['hiding_message']);
				}
				if (isset($categoryVisibility['redirect'])) {
					$settings['category_redirect'] =  array('value' => $categoryVisibility['redirect']);
				}
				if (isset($categoryVisibility['restriction_message'])) {
					$settings['category_restriction_message'] =  array('value' => $categoryVisibility['restriction_message']);
				}
                $this->validateDates($settings, array('category_restriction_begin', 'category_restriction_end'));
				if ($checkStore) {
					$scope = 'store';
				} else {
					$scope = 'default';
				}

				Mage::getModel('itoris_productpricevisibility/settings')->save($settings, $scope, $checkStore, 0, $categoryId);

				$resource = Mage::getSingleton('core/resource');
				$connection = $resource->getConnection('read');
				$categoryGroupTable = $resource->getTableName('itoris_productpricevisibility_category_visibility_group');
				$connection->query("delete from {$categoryGroupTable} where category_id={$categoryId} and store_id={$checkStore}");
				$useDefault = Mage::app()->getRequest()->getParam('itoris_use_default');
				if (!isset($useDefault['category_user_groups'])) {
					if (!empty($categoryVisibility['groups'])) {
						$categoryUserGroup = $categoryVisibility['groups'];
						foreach ($categoryUserGroup as $group) {
							$connection->query("insert into {$categoryGroupTable} (category_id, group_id, store_id) values ({$categoryId}, {$group}, {$checkStore})");
						}
					} else {
						$connection->query("insert into {$categoryGroupTable} (category_id, group_id, store_id) values ({$categoryId}, null, {$checkStore})");
					}
				}

			}
		}
	}

    protected function validateDates(&$data, $types) {
        foreach ($types as $type) {
            if (isset($data[$type]['value']) && $data[$type]['value']) {
                $parts = explode('-', $data[$type]['value']);
                if (count($parts) == 3) {
                    if (strlen($parts[0]) == 4 && intval($parts[0]) > 999) {
                        $month = (int)$parts[1];
                        $day = (int)$parts[2];
                        if ($month && $day) {
                            $parts[1] = $month < 10 ? '0' . $month : $month;
                            $parts[2] = $day < 10 ? '0' . $day : $day;
                            $data[$type]['value'] = implode('-', $parts);
                        } else {
                            $data[$type]['value'] = null;
                        }
                    } else {
                        $data[$type]['value'] = null;
                    }
                } else {
                    $data[$type]['value'] = null;
                }
            }

        }
        return $this;
    }

	/**
	 * @return Itoris_ProductPriceVisibility_Helper_Data
	 */

	public function getDataHelper() {
		return Mage::helper('itoris_productpricevisibility/data');
	}

}

?>