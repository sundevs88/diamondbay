<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PRODUCTPRICEVISIBILITY
 * @copyright  Copyright (c) 2013 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

 

class Itoris_ProductPriceVisibility_Model_Product_PrepareCollection_Observer {

	protected $categoryCollection = null;
	protected $visibilityCategoryCollection = null;
	protected $visibilityProductCollection = null;
	protected $visibilityPriceCollection = null;

    protected $settings = null;
    protected $priceGroupIdByProductId = null;
    protected $productGroupIdByProductId = null;

	public function beforeLoad($obj) {
		/** @var $collection Mage_Catalog_Model_Resource_Product_Collection */
		$collection = $obj->getCollection();
		if ($this->getDataHelper()->isRegisteredFrontend() && !Mage::registry('current_product')) {
			if (method_exists($collection, 'addCategoryIds')) {
				$collection->addCategoryIds();
			}
			$checkStore = (int)Mage::app()->getStore()->getId();
			/** @var $productVisibilityModel  Itoris_ProductPriceVisibility_Model_Settings */
			$productVisibilityModel = Mage::getModel('itoris_productpricevisibility/settings');
            $loadedSettings = $productVisibilityModel->loadSettingsForProduct(Mage::app()->getWebsite()->getId(), Mage::app()->getStore()->getId());
            $this->settings = $loadedSettings;
            $productIdHide = array();
            $categoryIdsByProductId = array();
            $resource = Mage::getSingleton('core/resource');
			/** @var $connection Varien_Db_Adapter_Pdo_Mysql */
			$connection = $resource->getConnection('read');
			$categoryTable = $resource->getTableName('catalog_category_product_index');

			$collectionSelect = $collection->getSelectCountSql();
            if (is_string($collectionSelect)) {
                $query = str_replace('count(e.entity_id)', 'e.entity_id', $collectionSelect);
            } else {
                $collectionSelect->reset(Zend_Db_Select::COLUMNS);
                $collectionSelect->columns('e.entity_id');
                $query = (string)$collectionSelect;
            }

			$productIds = $connection->fetchCol($query);
			$productIds = array_unique($productIds);
			$productIds = array_map('intval', $productIds);
			if (empty($productIds)) {
				return;
			}
            $priceGroupIdByProductId = $this->getDataHelper()->getGroupIdsProducts('itoris_productpricevisibility_price_visibility_group', $productIds, $checkStore);
            $productGroupIdByProductId = $this->getDataHelper()->getGroupIdsProducts('itoris_productpricevisibility_product_visibility_group', $productIds, $checkStore);
            $this->priceGroupIdByProductId = $priceGroupIdByProductId;
            $this->productGroupIdByProductId = $productGroupIdByProductId;

            $productIdsStr = implode(',', $productIds);
			$categoryIdsFromTable = $connection->fetchAll("select category_id, product_id from {$categoryTable} where product_id in ({$productIdsStr}) and is_parent = 1");
            foreach ($categoryIdsFromTable as $row) {
				if (!isset($categoryIdsByProductId[$row['product_id']])) {
					$categoryIdsByProductId[$row['product_id']] = array();
				}
				if (!in_array($row['category_id'], $categoryIdsByProductId[$row['product_id']])) {
					$categoryIdsByProductId[$row['product_id']][] = $row['category_id'];
				}
			}
            $idProductVisible = array();
			foreach ($productIds as $productId) {
				$categoryIds = array();

				$settings = new Varien_Object();
				if (isset($loadedSettings[$productId])) {
					$settings->addData($loadedSettings[$productId]);
				}
				if (isset($categoryIdsByProductId[$productId])) {
					$categoryIds = $categoryIdsByProductId[$productId];
				}

				if ($this->getDataHelper()->rulesForCategory($categoryIds, $checkStore, 'hide')) {
					if (!isset($priceGroupIdByProductId[$productId])) {
						continue;
					}
					$priceGroupId = $priceGroupIdByProductId[$productId];
					if (!$this->getDataHelper()->customerGroup($priceGroupId)
						&& $this->getDataHelper()->isVisibleByRestrictionDate($settings->getPriceRestrictionBegin(), $settings->getPriceRestrictionEnd())
					) {
						$productIdHide[] = $productId;
					}
				} else if ($this->getDataHelper()->rulesForCategory($categoryIds, $checkStore, 'out_of_stock') || $this->getDataHelper()->rulesForCategory($categoryIds, $checkStore, 'price')) {
                    $idProductVisible[] = $productId;
                }
			}

			foreach ($loadedSettings as $id => $product) {
				$productId = (int)$id;
				$product = new Varien_Object($product);
				if (!isset($productGroupIdByProductId[$productId])) {
					continue;
				}
                $productGroupId = $productGroupIdByProductId[$productId];
				if ($product->getData('product_hiding_mode') == Itoris_ProductPriceVisibility_Block_Admin_Product_Edit_Tab_PriceVisibility::PRODUCT_HIDE_COMPLETELY
					&& $this->getDataHelper()->customerGroup($productGroupId)
					&& $this->getDataHelper()->isVisibleByRestrictionDate($product->getData('product_restriction_begin'), $product->getData('product_restriction_end'))
				) {
					$productIdHide[] = $productId;
				} elseif ($this->getDataHelper()->customerGroup($productGroupId)
                    && $this->getDataHelper()->isVisibleByRestrictionDate($product->getData('product_restriction_begin'), $product->getData('product_restriction_end'))) {
                    $idProductVisible[] = $productId;
                }
			}
            $globalSettings = $productVisibilityModel->load(0, $checkStore);
            $globalGroupId = $globalSettings->getGlobalUserGroups();
            $globalAllHide = (
                ($globalSettings->getGlobalHidingMode() == Itoris_ProductPriceVisibility_Model_Settings::HIDE_ALL_GLOBAL
                    || $globalSettings->getGlobalHidingMode() == Itoris_ProductPriceVisibility_Model_Settings::HIDE_PRODUCT_GLOBAL
                )
                && $this->getDataHelper()->customerGroup($globalGroupId)
                && $this->getDataHelper()->isVisibleByRestrictionDate($globalSettings->getGlobalRestrictionBegin(), $globalSettings->getGlobalRestrictionEnd())
            );

            if ($globalAllHide) {
                $productIdHide = $collection->getAllIds();
                if (!empty($idProductVisible)) {
                    foreach ($idProductVisible as $id) {
                        $key = array_search($id, $productIdHide);
                        if ($key || $key == 0) {
                            unset($productIdHide[$key]);
                        }
                    }
                }
            }
			if ($collection instanceof Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Link_Product_Collection || $collection instanceof Mage_Catalog_Model_Resource_Product_Link_Product_Collection) {
				$request = Mage::app()->getRequest();
				if ($request->getRouteName() == 'checkout' && $request->getControllerName() == 'cart' && $request->getActionName() == 'index') {
					foreach ($productIds as $productId) {
						$categoryIds = array();
						$settings = new Varien_Object();
						if (isset($loadedSettings[$productId])) {
							$settings->addData($loadedSettings[$productId]);
						}

						if (isset($categoryIdsByProductId[$productId])) {
							$categoryIds = $categoryIdsByProductId[$productId];
						}

						if ($this->getDataHelper()->rulesForCategory($categoryIds, $checkStore, 'price') || $this->getDataHelper()->rulesForCategory($categoryIds, $checkStore, 'out_of_stock')) {
							if (!isset($priceGroupIdByProductId[$productId])) {
								continue;
							}
							$priceGroupId = $priceGroupIdByProductId[$productId];
							if (!$this->getDataHelper()->customerGroup($priceGroupId)
								&& $this->getDataHelper()->isVisibleByRestrictionDate($settings->getPriceRestrictionBegin(), $settings->getPriceRestrictionEnd())
							) {
								$productIdHide[] = $productId;
							}
						}
					}
					foreach ($loadedSettings as $productId => $product) {
						$product = new Varien_Object($product);
						if (!isset($productGroupIdByProductId[$productId])) {
							continue;
						}
						$productGroupId = $productGroupIdByProductId[$productId];
						if (($product->getData('product_hiding_mode') == Itoris_ProductPriceVisibility_Block_Admin_Product_Edit_Tab_PriceVisibility::PRODUCT_SHOW_OUT_OF_STOCK
							|| $product->getData('product_hiding_mode') == Itoris_ProductPriceVisibility_Block_Admin_Product_Edit_Tab_PriceVisibility::PRODUCT_NO_PRICE
						)
							&& $this->getDataHelper()->customerGroup($productGroupId)
							&& $this->getDataHelper()->isVisibleByRestrictionDate($product->getData('product_restriction_begin'), $product->getData('product_restriction_end'))
						) {
							$productIdHide[] = $productId;
						}

						foreach ($loadedSettings as $prodId => $prod) {
							$prod = new Varien_Object($prod);
							if (!isset($priceGroupIdByProductId[$prodId])) {
								continue;
							}
							$priceGroupId = $priceGroupIdByProductId[$productId];
							if (($prod->getData('price_hiding_mode') == Itoris_ProductPriceVisibility_Block_Admin_Product_Edit_Tab_PriceVisibility::PRICE_SHOW_OUT_OF_STOCK
								|| $prod->getData('price_hiding_mode') == Itoris_ProductPriceVisibility_Block_Admin_Product_Edit_Tab_PriceVisibility::PRICE_NO_PRICE
								|| $prod->getData('price_hiding_mode') == Itoris_ProductPriceVisibility_Block_Admin_Product_Edit_Tab_PriceVisibility::SHOW_CUSTOM_MESSAGE
								|| $prod->getData('price_hiding_mode') == Itoris_ProductPriceVisibility_Block_Admin_Product_Edit_Tab_PriceVisibility::SHOW_PRICE_DISALLOW_ADD_TO_CART
							)
								&& $this->getDataHelper()->customerGroup($priceGroupId)
								&& $this->getDataHelper()->isVisibleByRestrictionDate($prod->getData('price_restriction_begin'), $prod->getData('price_restriction_end'))
							) {
								$productIdHide[] = $prodId;
							}
						}
					}
				}
			}

			if (!empty($productIdHide)) {
				$collection->addFieldToFilter('entity_id', array('nin' => $productIdHide));
			}
		}
	}

	public function hideCurrentProduct($obj) {
		$product = $obj->getProduct();
		if ($this->getDataHelper()->isRegisteredFrontend()) {
			$checkStore = (int)Mage::app()->getStore()->getId();
			$groupId = $this->getDataHelper()->getGroupId('itoris_productpricevisibility_product_visibility_group', $product->getId(), $checkStore);
			$productVisibilityModel = Mage::getModel('itoris_productpricevisibility/settings');
			$productVisibility = $productVisibilityModel->load(0, $checkStore, $product->getId());
			if ($productVisibility->getProductHidingMode() == Itoris_ProductPriceVisibility_Block_Admin_Product_Edit_Tab_PriceVisibility::PRODUCT_HIDE_COMPLETELY
				&& $this->getDataHelper()->isRightConditions($productVisibility, $groupId)
			) {
                $redirectUrl = Mage::getStoreConfig(Mage_Cms_Helper_Page::XML_PATH_NO_ROUTE_PAGE);
                Mage::app()->getResponse()->setRedirect("$redirectUrl", 404);
                return;
			} else {
				$categoryIds = $product->getCategoryIds();
				if ($this->getDataHelper()->rulesForCategory($categoryIds, $checkStore, 'hide')) {
					$priceGroupId = $this->getDataHelper()->getGroupId('itoris_productpricevisibility_price_visibility_group', $product->getId(), $checkStore);
					if (!$this->getDataHelper()->customerGroup($priceGroupId)
						&& $this->getDataHelper()->isVisibleByRestrictionDate($productVisibility->getPriceRestrictionBegin(), $productVisibility->getPriceRestrictionEnd())
					) {
                        $redirectUrl = Mage::getStoreConfig(Mage_Cms_Helper_Page::XML_PATH_NO_ROUTE_PAGE);
                        Mage::app()->getResponse()->setRedirect("$redirectUrl", 404);
                        return;
					}
				}
			}
            $globalSettings = $productVisibilityModel->load(0, $checkStore);
            $globalGroupId = $globalSettings->getGlobalUserGroups();
            $globalAllHide = (
                ($globalSettings->getGlobalHidingMode() == Itoris_ProductPriceVisibility_Model_Settings::HIDE_ALL_GLOBAL
                || $globalSettings->getGlobalHidingMode() == Itoris_ProductPriceVisibility_Model_Settings::HIDE_PRODUCT_GLOBAL
                )
                && $this->getDataHelper()->customerGroup($globalGroupId)
                && $this->getDataHelper()->isVisibleByRestrictionDate($globalSettings->getGlobalRestrictionBegin(), $globalSettings->getGlobalRestrictionEnd())
            );
            if ($globalAllHide &&
                !($this->getDataHelper()->rulesForCategory($categoryIds, $checkStore, 'out_of_stock') || $this->getDataHelper()->rulesForCategory($categoryIds, $checkStore, 'price'))
            ) {
                $redirectUrl = Mage::getStoreConfig(Mage_Cms_Helper_Page::XML_PATH_NO_ROUTE_PAGE);
                Mage::app()->getResponse()->setRedirect("$redirectUrl", 404);
            }
		}
	}

	protected function getCategoryCollection() {
		if (is_null($this->categoryCollection)) {
			return $this->categoryCollection = Mage::getModel('catalog/category')->getCollection();
		} else {
			return $this->categoryCollection;
		}
	}

	protected function getVisibilityProductCollection() {
		if (is_null($this->visibilityProductCollection)) {
			return $this->visibilityProductCollection = Mage::getModel('itoris_productpricevisibility/settings');
		} else {
			return $this->visibilityProductCollection;
		}
	}

	public function afterLoad($object) {
		$collection = $object->getCollection();
		if ($this->getDataHelper()->isRegisteredFrontend()) {
			$checkStore = (int)Mage::app()->getStore()->getId();

			if (method_exists($collection, 'addCategoryIds')) {
				$collection->addCategoryIds();
			}
			$allProducts = $collection->getItems();
			foreach ($allProducts as $value) {
				$categoryIds = $value->getCategoryIds();
                if ($this->getDataHelper()->rulesForCategory($categoryIds, $checkStore, 'out_of_stock')) {
					$value->setIsSalable(false);
				}
                $settings = new Varien_Object();
                if (isset($this->settings[$value->getId()])) {
                    $settings->addData($this->settings[$value->getId()]);
                }
				$productGroupId = isset($this->productGroupIdByProductId[$value->getId()]) ? $this->productGroupIdByProductId[$value->getId()] : null;
				$priceGroupId = isset($this->priceGroupIdByProductId[$value->getId()]) ? $this->priceGroupIdByProductId[$value->getId()] : null;
				if ($this->getDataHelper()->isRightConditions($settings, $productGroupId) && $settings->getProductHidingMode() == Itoris_ProductPriceVisibility_Block_Admin_Product_Edit_Tab_PriceVisibility::PRODUCT_SHOW_OUT_OF_STOCK) {
					$value->setIsSalable(false);
				} elseif ($this->getDataHelper()->isRightConditions($settings, $priceGroupId, 'price') && $settings->getPriceHidingMode() == Itoris_ProductPriceVisibility_Block_Admin_Product_Edit_Tab_PriceVisibility::PRICE_SHOW_OUT_OF_STOCK) {
					$value->setIsSalable(false);
				}
			}
		}
	}

	public function afterLoadCurrentProduct($obj) {
		$product = $obj->getProduct();
		if ($this->getDataHelper()->isRegisteredFrontend()) {
			$checkStore = (int)Mage::app()->getStore()->getId();
			$productVisibilityCollection = $this->getVisibilityProductCollection();
			$currentProductId = (int)$product->getId();
			$categoryIds = $product->getCategoryIds();
			if ($this->getDataHelper()->rulesForCategory($categoryIds, $checkStore, 'out_of_stock')) {
				$product->setIsSalable(false);
			}
			$productModel = $productVisibilityCollection->load(0, $checkStore, $currentProductId);
			$productGroupId = $this->getDataHelper()->getGroupId('itoris_productpricevisibility_product_visibility_group', $currentProductId, $checkStore);
			$priceGroupId = $this->getDataHelper()->getGroupId('itoris_productpricevisibility_price_visibility_group', $currentProductId, $checkStore);
            $globalSettings = $productVisibilityCollection->load(0, $checkStore);
            $globalGroupId = $globalSettings->getGlobalUserGroups();
            $global = ($globalSettings->getGlobalHidingMode() == Itoris_ProductPriceVisibility_Model_Settings::SHOW_OUT_OF_STOCK_GLOBAL
                && $this->getDataHelper()->customerGroup($globalGroupId)
                && $this->getDataHelper()->isVisibleByRestrictionDate($globalSettings->getGlobalRestrictionBegin(), $globalSettings->getGlobalRestrictionEnd())
            );
			if ($this->getDataHelper()->isRightConditions($productModel, $productGroupId)) {
				if ($productModel->getProductHidingMode() == Itoris_ProductPriceVisibility_Block_Admin_Product_Edit_Tab_PriceVisibility::PRODUCT_SHOW_OUT_OF_STOCK) {
					$product->setIsSalable(false);
				}
			} elseif ($this->getDataHelper()->isRightConditions($productModel, $priceGroupId, 'price')) {
				if ($productModel->getPriceHidingMode() == Itoris_ProductPriceVisibility_Block_Admin_Product_Edit_Tab_PriceVisibility::PRICE_SHOW_OUT_OF_STOCK) {
					$product->setIsSalable(false);
				}
			} elseif ($global) {
                $product->setIsSalable(false);
            }
		}
	}



	/**
	 * @return Itoris_ProductPriceVisibility_Helper_Data
	 */

	public function getDataHelper() {
		return Mage::helper('itoris_productpricevisibility/data');
	}
}
?>