<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PRODUCTPRICEVISIBILITY
 * @copyright  Copyright (c) 2013 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

 

class Itoris_ProductPriceVisibility_Model_Observer {

	public function saveProduct($obj) {
		$products = $obj->getDataObject();
		if($this->getDataHelper()->getSettings()->getEnabled() && $this->getDataHelper()->isAdminRegistered()) {
			$productId = $products->getId();
			if ($productId) {
				$checkStore = (int)Mage::app()->getRequest()->getParam('store');
				$productRules =  Mage::app()->getRequest()->getParam('itoris_product_rules');
				$priceRules =  Mage::app()->getRequest()->getParam('itoris_price_rules');
				$settings = array();
				if (isset($productRules['restriction_begin'])) {
					$settings['product_restriction_begin'] =  array('value' => $productRules['restriction_begin']);
				}
				if (isset($productRules['restriction_end'])) {
					$settings['product_restriction_end'] =  array('value' => $productRules['restriction_end']);
				}
				if (isset($productRules['hiding_mode'])) {
					$settings['product_hiding_mode'] =  array('value' => $productRules['hiding_mode']);
				}

				if (isset($priceRules['restriction_begin'])) {
					$settings['price_restriction_begin'] =  array('value' => $priceRules['restriction_begin']);
				}
				if (isset($priceRules['restriction_end'])) {
					$settings['price_restriction_end'] =  array('value' => $priceRules['restriction_end']);
				}
				if (isset($priceRules['hiding_mode'])) {
					$settings['price_hiding_mode'] =  array('value' => $priceRules['hiding_mode']);
				}
				if (isset($priceRules['hiding_message'])) {
					$settings['price_hiding_message'] =  array('value' => $priceRules['hiding_message']);
				}
				if (isset($priceRules['restriction_message'])) {
					$settings['price_restriction_message'] =  array('value' => $priceRules['restriction_message']);
				}

                $this->validateDates($settings, array('product_restriction_begin', 'product_restriction_end', 'price_restriction_begin', 'price_restriction_end'));

				if ($checkStore) {
					$scope = 'store';
				} else {
					$scope = 'default';
				}

				Mage::getModel('itoris_productpricevisibility/settings')->save($settings, $scope, $checkStore, $productId);

				$resource = Mage::getSingleton('core/resource');
				$connection = $resource->getConnection('read');
				$productGroupTable = $resource->getTableName('itoris_productpricevisibility_product_visibility_group');
				$connection->query("delete from {$productGroupTable} where product_id={$productId} and store_id={$checkStore}");
				$useDefault = Mage::app()->getRequest()->getParam('itoris_use_default');
				if (!isset($useDefault['product_user_groups'])) {
					if (!empty($productRules['groups'])) {
						$productUserGroup = $productRules['groups'];
						foreach ($productUserGroup as $group) {
							$connection->query("insert into {$productGroupTable} (product_id, group_id, store_id) values ({$productId}, {$group}, {$checkStore})");
						}
					} else {
						$connection->query("insert into {$productGroupTable} (product_id, group_id, store_id) values ({$productId}, null, {$checkStore})");
					}
				}
				$priceGroupTable = $resource->getTableName('itoris_productpricevisibility_price_visibility_group');
				$connection->query("delete from {$priceGroupTable} where product_id={$productId} and store_id={$checkStore}");
				if (!isset($useDefault['price_user_groups'])) {
					if (!empty($priceRules['groups'])) {
						$priceUserGroup = $priceRules['groups'];
						foreach ($priceUserGroup as $group) {
							$connection->query("insert into {$priceGroupTable} (product_id, group_id, store_id) values ({$productId}, {$group}, {$checkStore})");
						}
					}else {
						$connection->query("insert into {$priceGroupTable} (product_id, group_id, store_id) values ({$productId}, null, {$checkStore})");
					}
				}
			}
		}
	}

    protected function validateDates(&$data, $types) {
        foreach ($types as $type) {
            if (isset($data[$type]['value']) && $data[$type]['value']) {
                $parts = explode('-', $data[$type]['value']);
                if (count($parts) == 3) {
                    if (strlen($parts[0]) == 4 && intval($parts[0]) > 999) {
                        $month = (int)$parts[1];
                        $day = (int)$parts[2];
                        if ($month && $day) {
                            $parts[1] = $month < 10 ? '0' . $month : $month;
                            $parts[2] = $day < 10 ? '0' . $day : $day;
                            $data[$type]['value'] = implode('-', $parts);
                        } else {
                            $data[$type]['value'] = null;
                        }
                    } else {
                        $data[$type]['value'] = null;
                    }
                } else {
                    $data[$type]['value'] = null;
                }
            }

        }
        return $this;
    }

	/**
	 * @return Itoris_ProductPriceVisibility_Helper_Data
	 */
	public function getDataHelper() {
		return Mage::helper('itoris_productpricevisibility');
	}

}

?>