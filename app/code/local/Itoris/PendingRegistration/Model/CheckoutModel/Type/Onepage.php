<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PENDINGREGISTRATION
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */
 
class Itoris_PendingRegistration_Model_CheckoutModel_Type_Onepage extends Mage_Checkout_Model_Type_Onepage{

	protected function _involveNewCustomer() {
		$customer = $this->getQuote()->getCustomer();

		/** @var $helper Itoris_PendingRegistration_Helper_Data */
		$helper = Mage::helper('itoris_pendingregistration/data');
		$helper->init(null);
		$scope = $helper->getFrontendScope();

		if (!Itoris_PendingRegistration_Model_Settings::inst()->isEngineActive($scope)) {
			return parent::_involveNewCustomer();
		}

		if (!$helper->isRegisteredAutonomous()) {
			return parent::_involveNewCustomer();
		}

		/** @var $db Varien_Db_Adapter_Pdo_Mysql */
		$db = Mage::getSingleton( 'core/resource' )->getConnection( 'core_write' );

		$usersTableName = Mage::getModel( 'core/resource' )->getTableName( 'itoris_pendingregistration_users' );
		$customersTableName = Mage::getModel( 'core/resource' )->getTableName( 'customer_entity' );
		$websiteId = (int)Mage::app()->getWebsite()->getId();
		$data = $db->fetchRow( 'SELECT * FROM '.$customersTableName.' WHERE email=? and website_id='.$websiteId,
							   $customer->getEmail()  );

		if (isset($data['email'])) {
			$id = intval($data['entity_id']);

			$isExists = (boolean) $db->fetchOne("SELECT COUNT(*) FROM ".$usersTableName." WHERE customer_id=?", $id);
			if (!$isExists) {
				$db->query("INSERT INTO $usersTableName SET customer_id=?, status=0, ip=?, date=CURRENT_TIMESTAMP()",
							array($id, $_SERVER[ 'REMOTE_ADDR' ]));

				$user = Mage::getModel('customer/customer')->load($id);

				$helper->sendEmail(Itoris_PendingRegistration_Model_Template::$EMAIL_REG_TO_ADMIN, $user, $scope);
				$helper->sendEmail(Itoris_PendingRegistration_Model_Template::$EMAIL_REG_TO_USER,  $user, $scope);
				
				Mage::getSingleton('customer/session')->addSuccess($helper
						->__('Thank you for registration. Your account requires moderation before you can login.'));
			}
		}
	}
	
}
?>