<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PENDINGREGISTRATION
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

class Itoris_PendingRegistration_Block_Admin_Form extends Mage_Adminhtml_Block_Widget_Form{
		
	protected function _prepareForm()
	{
		
		$form = new Varien_Data_Form();

		/** @var $fieldSet Varien_Data_Form_Element_Fieldset */
        $fieldSet = $form->addFieldset('template_fields', array(
				'legend' => $this->__('Template settings')
		));
		$fieldSet->setHeaderBar($this->getChildHtml('scopeToggle'));

		$fieldSet->addField('active', 'select', array(
				'name'  => 'active',
				'label' => $this->__('Is Active'),
				'title' => $this->__('Is Active'),
				'required' => true,
				'values' => array(
					0 => $this->__('No'),
					1 => $this->__('Yes')
				)
			)
		);

		if($this->getEmailTemplate()->getType() == Itoris_PendingRegistration_Model_Template::$EMAIL_REG_TO_ADMIN){
			$fieldSet->addField('admin_email', 'text', array(
					'name'  => 'admin_email',
					'label' => $this->__('Admin email'),
					'title' => $this->__('Admin email'),
					'required' => true,
				)
			);
		}

		$fieldSet->addField('from_name', 'text', array(
				'name'  => 'from_name',
				'label' => $this->__('From name'),
				'title' => $this->__('From name'),
				'required' => true,
			)
		);

		$fieldSet->addField('from_email', 'text', array(
				'name'  => 'from_email',
				'label' => $this->__('From email'),
				'title' => $this->__('From email'),
				'required' => true,
			)
		);

		$fieldSet->addField('subject', 'text', array(
				'name'  => 'subject',
				'label' => $this->__('Subject'),
				'title' => $this->__('Subject'),
				'required' => true,
			)
		);

		$fieldSet->addField('cc', 'text', array(
				'name'  => 'cc',
				'label' => $this->__('CC'),
				'title' => $this->__('CC'),
			)
		);

		$fieldSet->addField('bcc', 'text', array(
				'name'  => 'bcc',
				'label' => $this->__('BCC'),
				'title' => $this->__('BCC'),
			)
		);

		if(method_exists('Mage','getVersionInfo')){
			$wysiwygConfig = Mage::getSingleton('cms/wysiwyg_config')->getConfig(
				array(
				 'tab_id'      => $this->getTabId(),
				 'add_widgets'    => false,
				 'add_directives'   => true,
				 'files_browser_window_url'  => Mage::getSingleton('adminhtml/url')->getUrl('adminhtml/cms_wysiwyg_images/index'),
				 'directives_url'   => Mage::getSingleton('adminhtml/url')->getUrl('adminhtml/cms_wysiwyg/directive'),
				)
			);
		} else {
			$wysiwygConfig = false;
		}

		$fieldSet->addField('email_content', 'editor', array(
			'name'      => 'email_content',
			'id'		=> 'email_content',
			'label'     => $this->__('Template Content'),
			'title'     => $this->__('Template Content'),
			'required'  => true,
			'state'     => 'html',
			'style'     => 'height:36em;width:400px',
			'config'    => $wysiwygConfig,
		));

		$fieldSet->addField('email_styles', 'textarea', array(
				'name'  => 'email_styles',
				'label' => $this->__('Template Styles'),
				'title' => $this->__('Template Styles'),
			)
		);
		

		$form->setValues( $this->getEmailTemplate()->getData());

		$form->setAction($this->getUrl('*/*/save', array_merge(array('template'=> $this->getEmailTemplate()->getType()),
									$this->getParentBlock()->getScope()->getSummaryForUrl())));

		$form->setMethod( 'post' );
		$form->setUseContainer( true );
		$form->setId( 'edit_form' );
		$this->setForm($form);
		
		return parent::_prepareForm();
	}

	/**
	 * @return Itoris_PendingRegistration_Model_Template
	 */
	public function getEmailTemplate(){
		return $this->getParentBlock()->getEmailTemplate();
	}

	/**
	 * @return Itoris_PendingRegistration_Block_Admin_Container
	 */
	public function getParentBlock(){
		return parent::getParentBlock();
	}
}

?>