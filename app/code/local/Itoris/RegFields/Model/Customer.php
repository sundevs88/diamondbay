<?php
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_REGFIELDS
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

class Itoris_RegFields_Model_Customer extends Mage_Core_Model_Abstract {

	public function __construct() {
		$this->_init('itoris_regfields/customer');
	}

	/**
	 * Save customer custom fields values
	 *
	 * @param $options
	 * @param $customerId
	 * @return mixed
	 */
	public function saveOptions($options, $customerId) {
		if (empty($options)) {
			return;
		}
		$read = $this->getResource()->getReadConnection();
		foreach($options as $key => $option) {
			if (!($key == 'password' || $key == 'confirmation')) {
				if (!empty($option)) {
					if (is_array($option)) {
						$error = false;
						foreach ($option as $item) {
							if (is_array($item)) {
								$error = true;
								break;
							}
						}
						if (!$error) {
							$value = implode(',', $option);
						}
					} else {
						$value = $option;
					}
					if ($value == 'itoris_field_has_file') {
						continue;
					}
					$select = $read->select()
								   ->from($this->getResource()->getMainTable())
								   ->where('`customer_id`=?', $customerId, Zend_Db::INT_TYPE)
								   ->where('`key`=?', $key);
					$data = $read->fetchRow($select);
					if ($data) {
						$this->setData($data);
					} else {
						$this->setCustomerId($customerId);
						$this->setKey($key);
					}
					$this->setValue($value);
					$this->save();
					$this->unsetData();
				}
			}
		}
	}

	/**
	 * Load custom fields values for customer
	 *
	 * @param $key
	 * @param $customerId
	 * @return mixed
	 */
	public function loadOption($key, $customerId) {
		$read = $this->getResource()->getReadConnection();
		$select = $read->select()
					   ->from($this->getResource()->getMainTable())
					   ->where('`customer_id`=?', $customerId, Zend_Db::INT_TYPE)
					   ->where('`key`=?', $key);
		$data = $read->fetchRow($select);
		if ($data) {
			$this->setData($data);
		}
		return $this->getValue();
	}

	public function loadOptionsByCustomerId($customerId) {
		$read = $this->getResource()->getReadConnection();
		$select = $read->select()
			->from($this->getResource()->getMainTable())
			->where('`customer_id`=?', $customerId, Zend_Db::INT_TYPE);

		$values = $read->fetchAll($select);
		$result = array();
		foreach ($values as $value) {
			$result[$value['key']] = $value['value'];
		}

		return $result;
	}

	/**
	 * Add custom options to customer model
	 *
	 * @param $customer Mage_Customer_Model_Customer
	 * @return Itoris_RegFields_Model_Customer
	 */
	public function addOptionsToCustomer($customer) {
		if ($customer->getId()) {
			$options = $this->loadOptionsByCustomerId($customer->getId());
			foreach ($options as $code => $value) {
				if (!$customer->getData($code)) {
					$customer->setData($code, $value);
				}
			}
		}
		return $this;
	}

	/**
	 * @return Itoris_RegFields_Helper_Field
	 */
	private function getFieldHelper() {
		return Mage::helper('itoris_regfields/field');
	}

}
?>