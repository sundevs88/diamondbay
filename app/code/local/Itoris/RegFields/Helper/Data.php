<?php
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_REGFIELDS
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

class Itoris_RegFields_Helper_Data extends Mage_Core_Helper_Abstract {

	protected $alias = 'registration_fields';

	public function isAdminRegistered() {
		try{
			return true;
		} catch(Exception $e) {
			Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
			return false;
		}
	}

	public function isRegisteredAutonomous($website = null) {
		return true;
	}

	public function registerCurrentStoreHost($sn) {
		return true;
	}

	public function isRegistered($website) {
		return true;
	}

	public function getAlias() {
		return $this->alias;
	}

	/**
	 * If RegFields enabled for the store return settings for regFields, else return null
	 *
	 * @param null|int $websiteId
	 * @param null|int $storeId
	 * @return Itoris_RegFields_Model_Settings|null
	 */
	public function isEnabled($websiteId = null, $storeId = null) {
		/** @var $settingsModel Itoris_RegFields_Model_Settings */
		$settingsModel = Mage::getModel('itoris_regfields/settings');
		if (!$websiteId || !$storeId) {
			$websiteId = Mage::app()->getWebsite()->getId();
			$storeId = Mage::app()->getStore()->getId();
		}
		$settingsModel->load($websiteId, $storeId);
		if ($settingsModel->getEnable() == Itoris_RegFields_Model_Settings::ENABLED) {
			return $settingsModel;
		} else {
			return null;
		}
	}

	/**
	 * Upload file by file id in $_FILES
	 *
	 * @param $fileId
	 * @return array
	 */
	public function uploadFiles($fileId) {
	        try {
	            $uploader = new Varien_File_Uploader($fileId);
	            $uploader->setAllowRenameFiles(true);
	            $uploader->setFilesDispersion(true);
				$dir = Mage::getBaseDir('media') . DS . 'customer';
				if (!is_dir($dir)) {
					mkdir($dir);
				}
				$dir .=  DS . 'itoris';
				if (!is_dir($dir)) {
					mkdir($dir);
				}
	            $result = $uploader->save($dir);

	            $result['url'] = Mage::getBaseUrl('media') . 'customer/itoris/' . $result['file'];

	        } catch (Exception $e) {
	            $result = array(
	                'error' => $this->__('Cannot upload file.'),
				);
	        }
		return $result;
	}

	/**
	 * Get file from /media/customer/itoris/ by filename
	 *
	 * @param $fileName
	 */
	public function getFile($fileName) {
		$filePath = Mage::getBaseDir('media') . DS . 'customer' . DS . 'itoris' . $fileName;
		@ob_clean(); // clear output buffer
		 header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // some day in the past
		 header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		 header("Content-type: application/x-download");
		 header("Content-Transfer-Encoding: binary");
		 header("Accept-Ranges: bytes");
		 header("Content-Length: ".filesize($filePath));
		 header('Content-Disposition: attachment; filename="' . basename($filePath) . '"');
		 readfile($filePath);
		 exit;
	}

}
 
?>