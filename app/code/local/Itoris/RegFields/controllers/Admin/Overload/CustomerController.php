<?php
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_REGFIELDS
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

require_once Mage::getBaseDir() . DS . 'app' . DS . 'code' . DS . 'core' . DS . 'Mage' . DS . 'Adminhtml' . DS . 'controllers' . DS . 'CustomerController.php';

class Itoris_RegFields_Admin_Overload_CustomerController extends Mage_Adminhtml_CustomerController {

	public function editAction() {
        $this->_initCustomer();
        $this->loadLayout();

        /* @var $customer Mage_Customer_Model_Customer */
        $customer = Mage::registry('current_customer');

        // set entered data if was error when we do save
        $data = Mage::getSingleton('adminhtml/session')->getCustomerData(true);

        // restore data from SESSION
        if ($data) {
            $request = clone $this->getRequest();
            $request->setParams($data);

			$hasFormClass = file_exists(Mage::getBaseDir().DS.'app'.DS.'code'.DS.'core'.DS.'Mage'.DS.'Customer'.DS.'Model'.DS.'Form.php');

            if (isset($data['account'])) {
				if ($hasFormClass) {
					/* @var $customerForm Mage_Customer_Model_Form */
					$customerForm = Mage::getModel('customer/form');
					$customerForm->setEntity($customer)
						->setFormCode('adminhtml_customer')
						->setIsAjaxRequest(true);
					$formData = $customerForm->extractData($request, 'account');
					$customerForm->restoreData($formData);
				} else {
					$customer->addData($data['account']);
				}
            }

            if (isset($data['address']) && is_array($data['address'])) {
				if ($hasFormClass) {
					/* @var $addressForm Mage_Customer_Model_Form */
					$addressForm = Mage::getModel('customer/form');
					$addressForm->setFormCode('adminhtml_customer_address');

					foreach (array_keys($data['address']) as $addressId) {
						if ($addressId == '_template_') {
							continue;
						}

						$address = $customer->getAddressItemById($addressId);
						if (!$address) {
							$address = Mage::getModel('customer/address');
							$customer->addAddress($address);
						}

						$formData = $addressForm->setEntity($address)
							->extractData($request);
						$addressForm->restoreData($formData);
					}
				} else {
					foreach ($data['address'] as $addressId => $address) {
						 $addressModel = Mage::getModel('customer/address')->setData($address)
							 ->setId($addressId);
						 $customer->addAddress($addressModel);
					 }
				}
            }
        }

        $this->_title($customer->getId() ? $customer->getName() : $this->__('New Customer'));

        $this->_setActiveMenu('customer/new');

		$store = $customer->getStore();
		$websiteId = $store->getWebsiteId();
		$storeId = $store->getId();
		if ($this->getDataHelper()->isRegistered($store->getWebsite()) && $this->getDataHelper()->isEnabled($websiteId, $storeId)) {
			/** @var $tabsMenu Mage_Adminhtml_Block_Customer_Edit_Tabs */
			$tabsMenu = $this->getLayout()->getBlock('customer_edit_tabs');
			$tabsMenu->addTab('custom_options', array(
					'label'     => $this->__('Custom Registration Fields'),
					'content'   => $this->getLayout()->createBlock('itoris_regfields/admin_customer_edit_tab_information')->initForm()->toHtml(),
					'after'     => 'account',
					//'active'    => Mage::registry('current_customer')->getId() ? false : true
			));
		}

        $this->renderLayout();
    }

    public function saveAction() {
        $data = $this->getRequest()->getPost();
        if ($data) {
            $redirectBack   = $this->getRequest()->getParam('back', false);
            $this->_initCustomer('customer_id');
			$errors = true;
            /* @var $customer Mage_Customer_Model_Customer */
            $customer = Mage::registry('current_customer');

			$store = $customer->getStore();
			$websiteId = $store->getWebsiteId();
			$storeId = $store->getId();
			/** @var $settingsModel Itoris_RegFields_Model_Settings */
			$settingsModel = $this->getDataHelper()->isEnabled($websiteId, $storeId);
			if ($this->getDataHelper()->isRegistered($store->getWebsite()) && $settingsModel) {
			 	$params = $this->getRequest()->getParam('itoris');
				if ($_FILES && isset($_FILES['itoris'])) {
					 $fileItoris = $_FILES['itoris'];
					 foreach($fileItoris['name'] as $key => $fileName) {
						 if ($fileItoris['size'][$key]) {
							$file = $this->getDataHelper()->uploadFiles('itoris['.$key.']');
							 if ($file['error']) {
								 if (!is_array($errors)) {
									 $errors = array();
								 }
								 $errors[] = $file['error'];
							 } else {
								$params[$key] = serialize(array(
									'name' => $fileName,
									'file' => $file['file'],
									'size' => $fileItoris['size'][$key],
									'mime' => $fileItoris['type'][$key],
								));
							 }
						 }
					 }
				 }
				 /** @var $formModel Itoris_RegFields_Model_Form */
				 $formModel = Mage::getModel('itoris_regfields/form');
				 $formConfig = $formModel->getFormConfig($settingsModel->getActiveViewId($websiteId, $storeId));
				 $formValidation = Mage::helper('itoris_regfields/field')->validate($params, $formConfig, true);
				if (!empty($formValidation)) {
					if ($errors === true) {
						$errors = array();
					}
					$errors = array_merge($formValidation, $errors);
				}
			}

			$hasFormClass = file_exists(Mage::getBaseDir().DS.'app'.DS.'code'.DS.'core'.DS.'Mage'.DS.'Customer'.DS.'Model'.DS.'Form.php');
			if ($hasFormClass) {
				/* @var $customerForm Mage_Customer_Model_Form */
				$customerForm = Mage::getModel('customer/form');
				$customerForm->setEntity($customer)
					->setFormCode('adminhtml_customer')
					->ignoreInvisible(false)
				;

            	$formData = $customerForm->extractData($this->getRequest(), 'account');
            	$customerErrors = $customerForm->validateData($formData);
				if (is_array($customerErrors)) {
					if ($errors === true) {
						$errors = array();
					}
					$errors = array_merge($customerErrors, $errors);
				}
			} else {
				if (isset($data['account'])) {
					if (isset($data['account']['email'])) {
						$data['account']['email'] = trim($data['account']['email']);
					}
					$customer->addData($data['account']);
				}
			}
            if ($errors !== true) {
                foreach ($errors as $error) {
                    $this->_getSession()->addError($error);
                }
                $this->_getSession()->setCustomerData($data);
                $this->getResponse()->setRedirect($this->getUrl('*/customer/edit', array('id' => $customer->getId())));
                return;
            }

			if ($hasFormClass) {
	            $customerForm->compactData($formData);
			}

            // unset template data
            if (isset($data['address']['_template_'])) {
                unset($data['address']['_template_']);
            }

            $modifiedAddresses = array();
            if (!empty($data['address'])) {
				if ($hasFormClass) {
					/* @var $addressForm Mage_Customer_Model_Form */
					$addressForm = Mage::getModel('customer/form');
					$addressForm->setFormCode('adminhtml_customer_address')->ignoreInvisible(false);

					foreach (array_keys($data['address']) as $index) {
						$address = $customer->getAddressItemById($index);
						if (!$address) {
							$address   = Mage::getModel('customer/address');
						}

						$requestScope = sprintf('address/%s', $index);
						$formData = $addressForm->setEntity($address)
							->extractData($this->getRequest(), $requestScope);
						$errors   = $addressForm->validateData($formData);
						if ($errors !== true) {
							foreach ($errors as $error) {
								$this->_getSession()->addError($error);
							}
							$this->_getSession()->setCustomerData($data);
							$this->getResponse()->setRedirect($this->getUrl('*/customer/edit', array(
								'id' => $customer->getId())
							));
							return;
						}

						$addressForm->compactData($formData);

						// Set post_index for detect default billing and shipping addresses
						$address->setPostIndex($index);

						if ($address->getId()) {
							$modifiedAddresses[] = $address->getId();
						} else {
							$customer->addAddress($address);
						}
					}
				} else {
					foreach ($data['address'] as $index => $addressData) {
						 if (($address = $customer->getAddressItemById($index))) {
							 $addressId           = $index;
							 $modifiedAddresses[] = $index;
						 } else {
							 $address   = Mage::getModel('customer/address');
							 $addressId = null;
							 $customer->addAddress($address);
						 }

						 $address->setData($addressData)
								 ->setId($addressId)
								 ->setPostIndex($index); // We need set post_index for detect default addresses
					 }
				}
            }

            // default billing and shipping
            if (isset($data['account']['default_billing'])) {
                $customer->setData('default_billing', $data['account']['default_billing']);
            }
            if (isset($data['account']['default_shipping'])) {
                $customer->setData('default_shipping', $data['account']['default_shipping']);
            }
            if (isset($data['account']['confirmation'])) {
                $customer->setData('confirmation', $data['account']['confirmation']);
            }

            // not modified customer addresses mark for delete
            foreach ($customer->getAddressesCollection() as $customerAddress) {
                if ($customerAddress->getId() && !in_array($customerAddress->getId(), $modifiedAddresses)) {
                    $customerAddress->setData('_deleted', true);
                }
            }

            if (isset($data['subscription'])) {
                $customer->setIsSubscribed(true);
            } else {
                $customer->setIsSubscribed(false);
            }

            if (isset($data['account']['sendemail_store_id'])) {
                $customer->setSendemailStoreId($data['account']['sendemail_store_id']);
            }

            $isNewCustomer = !$customer->getId();
            try {
                $sendPassToEmail = false;
                // force new customer active
                if ($isNewCustomer) {
                    $customer->setPassword($data['account']['password']);
                    $customer->setForceConfirmed(true);
                    if ($customer->getPassword() == 'auto') {
                        $sendPassToEmail = true;
                        $customer->setPassword($customer->generatePassword());
                    }
                }

                Mage::dispatchEvent('adminhtml_customer_prepare_save', array(
                    'customer'  => $customer,
                    'request'   => $this->getRequest()
                ));

                $customer->save();

				if ($this->getDataHelper()->isRegisteredAutonomous($store->getWebsite()) && $this->getDataHelper()->isEnabled($websiteId, $storeId)) {
					 /** @var $customerOptions Itoris_RegFields_Model_Customer */
					 $customerOptions = Mage::getModel('itoris_regfields/customer');
					 $customerOptions->saveOptions($params, $customer->getId());
			 	}

                // send welcome email
                if ($customer->getWebsiteId() && (isset($data['account']['sendemail']) || $sendPassToEmail)) {
                    $storeId = $customer->getSendemailStoreId();
                    if ($isNewCustomer) {
                        $customer->sendNewAccountEmail('registered', '', $storeId);
                    }
                    // confirm not confirmed customer
                    else if ((!$customer->getConfirmation())) {
                        $customer->sendNewAccountEmail('confirmed', '', $storeId);
                    }
                }

                if (!empty($data['account']['new_password'])) {
                    $newPassword = $data['account']['new_password'];
                    if ($newPassword == 'auto') {
                        $newPassword = $customer->generatePassword();
                    }
                    $customer->changePassword($newPassword);
                    $customer->sendPasswordReminderEmail();
                }

                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__('The customer has been saved.')
                );
                Mage::dispatchEvent('adminhtml_customer_save_after', array(
                    'customer'  => $customer,
                    'request'   => $this->getRequest()
                ));

                if ($redirectBack) {
                    $this->_redirect('*/*/edit', array(
                        'id'    => $customer->getId(),
                        '_current'=>true
                    ));
                    return;
                }
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $this->_getSession()->setCustomerData($data);
                $this->getResponse()->setRedirect($this->getUrl('*/customer/edit', array('id' => $customer->getId())));
            } catch (Exception $e) {
				$this->_getSession()->addError($e->getMessage());
                $this->_getSession()->addException($e,
                    Mage::helper('adminhtml')->__('An error occurred while saving the customer.'));
                $this->_getSession()->setCustomerData($data);
                $this->getResponse()->setRedirect($this->getUrl('*/customer/edit', array('id'=>$customer->getId())));
                return;
            }
        }
        $this->getResponse()->setRedirect($this->getUrl('*/customer'));
    }

	/**
	 * @return Itoris_RegFields_Helper_Data
	 */
	private function getDataHelper() {
		return Mage::helper('itoris_regfields');
	}

}
?>