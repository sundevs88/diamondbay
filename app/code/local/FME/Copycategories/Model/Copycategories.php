<?php

class FME_Copycategories_Model_Copycategories extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('copycategories/copycategories');
    }
}