<?php
class FME_Copycategories_Block_Adminhtml_Catalog_Category_Edit extends Mage_Adminhtml_Block_Catalog_Category_Edit
{
    
	 public function __construct()
    {
    	parent::__construct();
    	     $this->setChild('update_button',
                $this->getLayout()->createBlock('adminhtml/widget_button')
                    ->setData(array(
                        'label'     => Mage::helper('catalog')->__('Update Category'),
                        'onclick'   => "categoryUpdate('" . $this->getUrl('*/*/delete', array('_current' => true)) . "', true, {$categoryId})",
                        'class' => 'delete'
                    ))
            );

    }
      public function getUpdateButtonHtml()
    {
        return $this->getChildHtml('update_button');
    }
 
}