<?php

class FME_Copycategories_Block_Adminhtml_Copycategories_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('copycategories_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('copycategories')->__('Item Information'));
  }

  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('copycategories')->__('Item Information'),
          'title'     => Mage::helper('copycategories')->__('Item Information'),
          'content'   => $this->getLayout()->createBlock('copycategories/adminhtml_copycategories_edit_tab_form')->toHtml(),
      ));
     
      return parent::_beforeToHtml();
  }
}