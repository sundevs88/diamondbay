<?php
class FME_Copycategories_Block_Adminhtml_Copycategories extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_copycategories';
    $this->_blockGroup = 'copycategories';
    $this->_headerText = Mage::helper('copycategories')->__('Item Manager');
    $this->_addButtonLabel = Mage::helper('copycategories')->__('Add Item');
    parent::__construct();
  }
}