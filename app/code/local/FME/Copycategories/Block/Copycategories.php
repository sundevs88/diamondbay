<?php
class FME_Copycategories_Block_Copycategories extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
     public function getCopycategories()     
     { 
        if (!$this->hasData('copycategories')) {
            $this->setData('copycategories', Mage::registry('copycategories'));
        }
        return $this->getData('copycategories');
        
    }
}