<?php

class FME_Copycategories_Helper_Data extends Mage_Core_Helper_Abstract
{
	public function setEditUrl()
    {
        return Mage::helper("adminhtml")->getUrl("*/*/edit", array('_current'=>true, 'store'=>null, '_query'=>false, 'id'=>null, 'parent'=>null));
    }
    public function getparentcate($cate_p,$cates,$arr)
    {
     
       		$category = Mage::getModel('catalog/category')->load($cate_p);
    	    $helper = Mage::helper('catalog/category');
            $childrenCategories = $category->getChildrenCategories();
       	 foreach ($childrenCategories as $category) {
 		if(in_array($category->getId(),$cates))
            {
                $arr[] =  $category->getId();
                
                $category = Mage::getModel( 'catalog/category' )->load($category->getId());
            	$copy = clone $category;
            	$copy->setId(null)->save();
           	 	$id = $copy->getId();       
           		$categoryy = Mage::getModel('catalog/category')->load($id);
           		$categoryy->move($cate_p,null);
           		$childrenCategori = $category->getChildrenCategories();
           		if(sizeof($childrenCategori)> 0 ){
           			$this->getparentcate($id,$cates,$arr);
           		}
           	}
        }
    }
}